
# About

Plugin for Neovim+Telescope to fuzzy search and show Zeal documentation
pages.

NOTE: This is a Fennel-based plugin and the instructions are Fennel-based.
Providing a pure Lua-based one is on the roadmap.

# Requirements

- [`hotpot.nvim`](https://github.com/rktjmp/hotpot.nvim) -- this plugin is implemented in Fennel, hotpot allows Neovim to load Fennel as if it was Lua
- [`telescope.nvim`](https://github.com/nvim-telescope/telescope.nvim) -- Great fuzzy searcher for Neovim
- [`zeal-cli`](https://gitlab.com/ivan-cukic/zeal-lynx-cli) -- script that shows Zeal pages in Lynx

# Usage

To show the pages available in `cpp` docset (see zeal-cli documentation for setting this up):

    :lua require'telescope_zeal'.show('cpp')

# Installation and configuration

Use your preferred package manager and call the setup function
with something along these lines:

    {
        :documentation_sets {
            :cpp { :title "C++ Reference" }
            :qt5 { :title "Qt 5 Documentation" }
            :qt6 { :title "Qt 6 Documentation" }
        }
    }

# Screenshot

![zeal](https://gitlab.com/manning-fpcpp-book/zeal-lynx-cli/-/wikis/uploads/af9817577bf8f696201489913e7a1a16/zeal.jpeg)

# License

GPL 2 or later

